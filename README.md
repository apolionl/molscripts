## Molscripts

This repository contains the input files for simulation of several organic solvents based on the OPLS force field, 
ready to use in the open source software LAMMPS. Molecule geometries and force field parameters were taken from 
[ligpargen](http://zarbi.chem.yale.edu/ligpargen/).


In order to use the scripts, [moltemplate](https://github.com/jewettaij/moltemplate) (load force field parameters) and [packmol](http://github.com/mcubeg/packmol) (build geometry thus, moltemplate will be able to link the force field parameters) must be installed.


Till this moment, only 5 organic solvents and qti4p water model have been scripted, namely : 

1. Ethanol.
2. Bromobenzene.
3. Dibromomethane. 	(Deviation to exp. density around 4%)
4. Chloroform.
5. CFC-11 		(It does not reproduce the density, error larger than 5%)
6. Water 		(qti4p).
7. Methanol
8. 1,2 Dibromoethane
---


## Using scripts 

The use is easy, just type in the terminal.


```sh
packmol < geometry						## build geometry box.xyz 
moltemplate.sh -atomstyle full -xyz box.xyz topology.lt		## LAMMPS data file
```


Box.xyz is the general name of the geometry given by packmol, while system.lt is the input file with the 
force field parameters and number of molecules.

