# This file contains a definition for the "Bromobenzene" molecular subunit.

# First, load the OPLS force field parameters we will need.
# These 2 files are located in the "force_fields" subdirectory
# distributed with moltemplate.

#import "oplsaa.lt"    # <-- defines the standard "OPLSAA" force field
#import "loplsaa.lt"   # <-- custom parameters for long alkane chains taken from
                      #     Sui et al. J.Chem.Theory.Comp (2012), 8, 1459
		      #     To use the ordinary OPLSAA force field parameters,
		      #     (instead of the Sui et al. parameters), change the
		      #     atom types below from "@atom:80L","@atom:85LCH3" to
		      #     "@atom:80" and "@atom:85"  (defined in "oplsaa.lt")



# Then define "Dibromomethane":


DBM  {




  ############ force Field parameters "In Settings" ##########################


  write_once('In Init') {
  #-----------initialize simulation------------
  units real                                        # dt = 0.001 fmsec and skin = 2.0 Bohr.  Remember distance in Borh !!!
  atom_style full                                   # bonds, angles, dihedrals, impropers & charge
  dimension    3
  boundary    p p p
  bond_style  harmonic
  angle_style harmonic
  dihedral_style  opls
  improper_style cvff
  pair_style lj/cut/coul/long 10.0 10.0
  pair_modify mix geometric
  special_bonds lj/coul 0.0 0.0 0.5
  kspace_style pppm 0.000001
  neighbor            2.0 bin
  neigh_modify delay 0 every 10 check yes
  # -------- Define Interatomic Potential ----
  # -------- Defining the 1-4 interaction ----
  # -------- OPLS weights the lj and coulomb interactions by 0.5 ------ special_bonds   lj/coul 0.0 0.0 0.5 angle no dihedral no
  ##### To compute pairwise interactions ############################
  ##### lj/cut/tip4p/long args = otype htype btype atype qdist cutoff (cutoff2)
  #####                          otype,htype = atom types for TIP4P O and H
  #####                          btype,atype = bond and angle types for TIP4P waters
  #####                          qdist = distance from O atom to massless charge (distance units)
  #####                          cutoff = global cutoff for LJ (and Coulombic if only 1 arg) (distance units)
  #####                          cutoff2 = global cutoff for Coulombic (optional) (distance units)
  ##### Use this for water QTIP4P
  ##### pair_style      lj/cut/tip4p/long 1 2 1 1 0.278072379 17.007
  ###################################################################
  }





  write("Data Atoms") {
  # atomID   molID    atomType     charge      coordX coordY  coordZ
  $atom:Br0  $mol:.   @atom:1    -0.16160000   1.000   1.000   0.000
  $atom:C01  $mol:.   @atom:2    0.00070000   -0.933   1.000   0.000
  $atom:Br2  $mol:.   @atom:3    -0.16130000  -1.700   1.000   1.775
  $atom:H03  $mol:.   @atom:4    0.16110000   -1.273   0.101  -0.518
  $atom:H04  $mol:.   @atom:5    0.16110000   -1.273   1.899  -0.518


  }

  write_once('Data Masses') {
   # atomType mass
   @atom:1    79.9040
   @atom:2    12.0110
   @atom:3    79.9040
   @atom:4    1.0080
   @atom:5    1.0080
  }


  write('Data Bonds') {
  # bondID     bondType       atomID1     atomID2
  $bond:1    @bond:1       $atom:C01   $atom:Br0
  $bond:2    @bond:2       $atom:Br2   $atom:C01
  $bond:3    @bond:3       $atom:H03   $atom:C01
  $bond:4    @bond:4       $atom:H04   $atom:C01

  }


  write('Data Angles') {
  # angleID    angleType       atomID1       atomID2      atomID3
  $angle:1     @angle:1 	     $atom:Br0     $atom:C01 	  $atom:Br2
  $angle:2 	   @angle:2     	 $atom:Br0     $atom:C01		  $atom:H03
  $angle:3 	   @angle:3      	$atom:Br0     $atom:C01		  $atom:H04
  $angle:4 	   @angle:4      	$atom:Br2     $atom:C01		  $atom:H03
  $angle:5 	   @angle:5      	$atom:Br2     $atom:C01 	  $atom:H04
  $angle:6 	   @angle:6      	$atom:H03     $atom:C01		  $atom:H04

  }

  write('Data Dihedrals') {
  # dihedralID    dihedralType     atomID1      atomID2       atomID3          atomID4

  }




  write('Data Impropers') {
  # improperID    improperType     atomID1      atomID2       atomID3          atomID4
  $improper:1 	  @improper:1  	   $atom:C01   $atom:Br0 	    $atom:Br2       $atom:H03
  $improper:2    @improper:2      $atom:C01   $atom:Br0		    $atom:Br2       $atom:H04


  }



    write_once('In Settings') {
    #-----------Non bounded pair interactions  Pair Coeffs ------------
    ##            Type                   Emin sigma; (1-4): Emin/2 sigma
    pair_coeff   @atom:1   @atom:1      0.470  3.4700000
    pair_coeff   @atom:2   @atom:2      0.066  3.5000000
    pair_coeff   @atom:3   @atom:3      0.470  3.4700000
    pair_coeff   @atom:4   @atom:4      0.030  2.5000000
    pair_coeff   @atom:5   @atom:5      0.030  2.5000000

    ##### -- Bonded interactions ---
    ### bondType parameter list (k_bond, r0)

    bond_coeff   @bond:1      245.0000     1.9450
    bond_coeff   @bond:2      245.0000     1.9450
    bond_coeff   @bond:3      340.0000     1.0900
    bond_coeff   @bond:4      340.0000     1.0900

    #### angleType parameter-list (k_theta, theta0) ####################

    angle_coeff     @angle:1     78.000    111.700
    angle_coeff     @angle:2     51.000    107.600
    angle_coeff     @angle:3     51.000    107.600
    angle_coeff     @angle:4     51.000    107.600
    angle_coeff     @angle:5     51.000    107.600
    angle_coeff     @angle:6     33.000    107.800

    #### Dihedralcoefficients dihedralType     parameter-list (k_theta, theta0) ####################


    #### Impropers    parameter-list (k_theta, theta0) ###########


    improper_coeff @improper:1  0.000      -1       2
    improper_coeff @improper:2  0.000      -1       2



      }


} # DBM
